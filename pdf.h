// format A4 en points
#define MM2PT ( 72.0 / 25.4 )
#define A4_W  ( 210.0 * MM2PT )
#define A4_H  ( 297.0 * MM2PT )

class pdfout {
public:
cairo_surface_t * surface;
cairo_t * cai;	// le cairo de la surface PDF
int landscape;	// 0 pour portrait
int mm_margin;	// marge en mm

// constructeur
pdfout() : landscape(0), mm_margin(5) {};
// methodes
int init( const char * fnam );
void draw_one_page( GdkPixbuf * pixbu );
void close();
};
