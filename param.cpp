using namespace std;
#include <string>
#include <vector>

#include <gtk/gtk.h>
#include <stdio.h>
// #include "modpop.h"

#include "batch.h"
#include "imago.h"
#include "pdf.h"
#include "param.h"
#include "gui.h"


/** GTK callbacks ------------------------------------ */

// pour le bouton X du bandeau, en fait on ne delete pas, on cache seult
static gint param_view_delete_call( GtkWidget *widget, GdkEvent *event, gpointer data )
{
gtk_widget_hide( widget );
return (TRUE);
}

// sliders
void k_call( GtkAdjustment *adjustment, glostru * glo )
{
if	( adjustment->value != glo->k )
	{
	glo->k = adjustment->value;
	glo->darea_queue_flag = 1;
	}
// printf("k = %g now\n", glo->k ); fflush(stdout);
}

void level_floor_call( GtkAdjustment *adjustment, glostru * glo )
{
if	( adjustment->value != (double)glo->img.in_Rlo )
	{
	glo->img.in_Rlo = (int)adjustment->value;
	glo->img.init3lut( 1 );
	glo->img.process();
	glo->darea_queue_flag = 1;
	}
}

void level_top_call( GtkAdjustment *adjustment, glostru * glo )
{
if	( adjustment->value != (double)glo->img.in_Rlo )
	{
	glo->img.in_Rhi = (int)adjustment->value;
	glo->img.init3lut( 1 );
	glo->img.process();
	glo->darea_queue_flag = 1;
	}
}

void gamma_call( GtkAdjustment *adjustment, glostru * glo )
{
if	( adjustment->value != 1.0/(double)glo->img.expon )
	{
	glo->img.expon = 1.0/adjustment->value;
	glo->img.init3lut( 1 );
	glo->img.process();
	glo->darea_queue_flag = 1;
	}
}

void crop_x0_call( GtkAdjustment *adjustment, glostru * glo )
{
if	( adjustment->value != (double)glo->img.x0 )
	{
	glo->img.x0 = (int)adjustment->value;
	glo->img.process();	glo->darea_queue_flag = 1;
	}
}

void crop_x1_call( GtkAdjustment *adjustment, glostru * glo )
{
if	( adjustment->value != (double)glo->img.x1 )
	{
	glo->img.x1 = (int)adjustment->value;
	glo->img.process();	glo->darea_queue_flag = 1;
	}
}

void crop_y0_call( GtkAdjustment *adjustment, glostru * glo )
{
if	( adjustment->value != (double)glo->img.y0 )
	{
	glo->img.y0 = (int)adjustment->value;
	glo->img.process();	glo->darea_queue_flag = 1;
	}
}

void crop_y1_call( GtkAdjustment *adjustment, glostru * glo )
{
if	( adjustment->value != (double)glo->img.y1 )
	{
	glo->img.y1 = (int)adjustment->value;
	glo->img.process();	glo->darea_queue_flag = 1;
	}
}

/** main methods --------------------------------------*/

void param_view::build()
{
#define glo (&theglo)
GtkWidget *curwidg;

// c'est une fenetre banale en fait, elle se declare comme la fenetre principale
// ses events sont traites par la main loop principale
// la seule difference c'est que son "delete_event" la cache au lieu de terminer l'application
curwidg = gtk_window_new( GTK_WINDOW_TOPLEVEL );/* DIALOG est deprecated, POPUP est autre chose */
gtk_window_set_modal( GTK_WINDOW(curwidg), FALSE );

gtk_signal_connect( GTK_OBJECT(curwidg), "delete_event",
                    GTK_SIGNAL_FUNC( param_view_delete_call ), NULL );

gtk_window_set_title( GTK_WINDOW(curwidg), "Parameters" );
gtk_container_set_border_width( GTK_CONTAINER(curwidg), 10 );
// set a minimum size
gtk_widget_set_size_request( curwidg, 640, 480 );
wmain = curwidg;

/* creer boite verticale */
curwidg = gtk_vbox_new( FALSE, 5 ); /* spacing ENTRE objets */
gtk_container_add( GTK_CONTAINER( wmain ), curwidg );
vsli = curwidg;

/* creer des sliders */
	{
	GtkWidget *hbox, *label, *hscale;
	GtkAdjustment *adjustment;

	// ----[]----
	hbox = gtk_hbox_new( FALSE, 4 );
	gtk_box_pack_start( GTK_BOX(vsli), hbox, FALSE, FALSE, 0);

	label = gtk_label_new("scale :");
	gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0);

				//value,lower,upper,step_increment,page_increment,page_size);
	adjustment = GTK_ADJUSTMENT( gtk_adjustment_new( glo->k, 0.10, 5.0, 0.05, 1.0, 0 ));
	adk = adjustment;
	g_signal_connect( adjustment, "value_changed",
			  G_CALLBACK(k_call), (gpointer)glo );

	hscale = gtk_hscale_new(adjustment);
	gtk_scale_set_digits( GTK_SCALE(hscale), 2 );
	gtk_box_pack_start( GTK_BOX(hbox), hscale, TRUE, TRUE, 0 );

	// ----[]----
	hbox = gtk_hbox_new( FALSE, 4 );
	gtk_box_pack_start( GTK_BOX(vsli), hbox, FALSE, FALSE, 0);

	label = gtk_label_new("level top :");
	gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0);

	adjustment = GTK_ADJUSTMENT( gtk_adjustment_new( glo->img.in_Rhi, 128.0, 255.0, 1.0, 1.0, 0 ));
	adtop = adjustment;
	g_signal_connect( adjustment, "value_changed",
			  G_CALLBACK(level_top_call), (gpointer)glo );

	hscale = gtk_hscale_new(adjustment);
	gtk_scale_set_digits( GTK_SCALE(hscale), 0 );
	gtk_box_pack_start( GTK_BOX(hbox), hscale, TRUE, TRUE, 0 );

	// ----[]----
	hbox = gtk_hbox_new( FALSE, 4 );
	gtk_box_pack_start( GTK_BOX(vsli), hbox, FALSE, FALSE, 0);

	label = gtk_label_new("gamma :");
	gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0);

	adjustment = GTK_ADJUSTMENT( gtk_adjustment_new( 1.0/glo->img.expon, 0.25, 4.0, 0.25, 0.25, 0 ));
	adgam = adjustment;
	g_signal_connect( adjustment, "value_changed",
			  G_CALLBACK(gamma_call), (gpointer)glo );

	hscale = gtk_hscale_new(adjustment);
	gtk_scale_set_digits( GTK_SCALE(hscale), 2 );
	gtk_box_pack_start( GTK_BOX(hbox), hscale, TRUE, TRUE, 0 );

	// ----[]----
	hbox = gtk_hbox_new( FALSE, 4 );
	gtk_box_pack_start( GTK_BOX(vsli), hbox, FALSE, FALSE, 0);

	label = gtk_label_new("level floor :");
	gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0);

	adjustment = GTK_ADJUSTMENT( gtk_adjustment_new( glo->img.in_Rlo, 0.0, 128.0, 1.0, 1.0, 0 ));
	adbot = adjustment;
	g_signal_connect( adjustment, "value_changed",
			  G_CALLBACK(level_floor_call), (gpointer)glo );

	hscale = gtk_hscale_new(adjustment);
	gtk_scale_set_digits( GTK_SCALE(hscale), 0 );
	gtk_box_pack_start( GTK_BOX(hbox), hscale, TRUE, TRUE, 0 );

	// ----[]----
	hbox = gtk_hbox_new( FALSE, 4 );
	gtk_box_pack_start( GTK_BOX(vsli), hbox, FALSE, FALSE, 0);

	label = gtk_label_new("marge L");
	gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0);

	adjustment = GTK_ADJUSTMENT( gtk_adjustment_new( glo->img.x0, 0.0, 720, 1.0, 1.0, 0 ));
	adx0 = adjustment;
	g_signal_connect( adjustment, "value_changed",
			  G_CALLBACK(crop_x0_call), (gpointer)glo );

	hscale = gtk_hscale_new(adjustment);
	gtk_scale_set_digits( GTK_SCALE(hscale), 0 );
	gtk_box_pack_start( GTK_BOX(hbox), hscale, TRUE, TRUE, 0 );


	// ----[]----
	hbox = gtk_hbox_new( FALSE, 4 );
	gtk_box_pack_start( GTK_BOX(vsli), hbox, FALSE, FALSE, 0);

	label = gtk_label_new("marge R");
	gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0);

	adjustment = GTK_ADJUSTMENT( gtk_adjustment_new( glo->img.x1, 0.0, 720, 1.0, 1.0, 0 ));
	adx1 = adjustment;
	g_signal_connect( adjustment, "value_changed",
			  G_CALLBACK(crop_x1_call), (gpointer)glo );

	hscale = gtk_hscale_new(adjustment);
	gtk_scale_set_digits( GTK_SCALE(hscale), 0 );
	gtk_box_pack_start( GTK_BOX(hbox), hscale, TRUE, TRUE, 0 );


	// ----[]----
	hbox = gtk_hbox_new( FALSE, 4 );
	gtk_box_pack_start( GTK_BOX(vsli), hbox, FALSE, FALSE, 0);

	label = gtk_label_new("marge T");
	gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0);

	adjustment = GTK_ADJUSTMENT( gtk_adjustment_new( glo->img.y0, 0.0, 720, 1.0, 1.0, 0 ));
	ady0 = adjustment;
	g_signal_connect( adjustment, "value_changed",
			  G_CALLBACK(crop_y0_call), (gpointer)glo );

	hscale = gtk_hscale_new(adjustment);
	gtk_scale_set_digits( GTK_SCALE(hscale), 0 );
	gtk_box_pack_start( GTK_BOX(hbox), hscale, TRUE, TRUE, 0 );


	// ----[]----
	hbox = gtk_hbox_new( FALSE, 4 );
	gtk_box_pack_start( GTK_BOX(vsli), hbox, FALSE, FALSE, 0);

	label = gtk_label_new("marge B");
	gtk_box_pack_start( GTK_BOX(hbox), label, FALSE, FALSE, 0);

	adjustment = GTK_ADJUSTMENT( gtk_adjustment_new( glo->img.y1, 0.0, 720, 1.0, 1.0, 0 ));
	ady1 = adjustment;
	g_signal_connect( adjustment, "value_changed",
			  G_CALLBACK(crop_y1_call), (gpointer)glo );

	hscale = gtk_hscale_new(adjustment);
	gtk_scale_set_digits( GTK_SCALE(hscale), 0 );
	gtk_box_pack_start( GTK_BOX(hbox), hscale, TRUE, TRUE, 0 );
	}

// on doit faire un show de tous les widgets sauf la top window
// solution peu elegante :
//	gtk_widget_show_all( wmain );
//	gtk_widget_hide( wmain );
// solution retenue: faire un show de tous les descendants de la top mais pas elle :
GList * momes = gtk_container_get_children( GTK_CONTAINER(wmain) );
while	( momes )
	{
	gtk_widget_show_all( GTK_WIDGET(momes->data) );
	momes = momes->next;
	}
}


void param_view::show()
{
if	( !wmain )
	build();
gtk_window_present( GTK_WINDOW(wmain) );
}

void param_view::hide()
{
gtk_widget_hide( wmain );
}
