# directories
# GTKBASE= F:/Appli/msys64/mingw32
# on ne depend plus d'un F: absolu, mais on doit compiler avec le shell mingw32
GTKBASE= /mingw32

# listes
SOURCESC =
SOURCESCPP = batch.cpp  gui.cpp  imago.cpp  param.cpp  pdf.cpp
HEADERS = batch.h  cli_parse.h  gui.h  imago.h  param.h  pdf.h

OBJS= batch.o  gui.o  imago.o  param.o  pdf.o

EXE = scanman.exe 
# maintenir les libs et includes dans l'ordre alphabetique SVP

# LIBS= `pkg-config --libs gtk+-2.0`
LIBS= -L$(GTKBASE)/lib \
-latk-1.0 \
-lcairo \
-lgdk-win32-2.0 \
-lgdk_pixbuf-2.0 \
-lglib-2.0 \
-lgmodule-2.0 \
-lgobject-2.0 \
-lgtk-win32-2.0 \
-lpango-1.0 \
-lpangocairo-1.0 \
-lpangowin32-1.0

# -mwindows
# enlever -mwindows pour avoir la console stdout

# options
# INCS= `pkg-config --cflags gtk+-2.0`
INCS= -Wall -Wno-parentheses -Wno-deprecated-declarations -O2 -mms-bitfields \
-I$(GTKBASE)/include \
-I$(GTKBASE)/include/atk-1.0 \
-I$(GTKBASE)/include/cairo \
-I$(GTKBASE)/include/gdk-pixbuf-2.0 \
-I$(GTKBASE)/include/glib-2.0 \
-I$(GTKBASE)/include/gtk-2.0 \
-I$(GTKBASE)/include/harfbuzz \
-I$(GTKBASE)/include/pango-1.0 \
-I$(GTKBASE)/lib/glib-2.0/include \
-I$(GTKBASE)/lib/gtk-2.0/include \
# cibles

ALL : $(OBJS)
	g++ -o $(EXE) -s $(OBJS) $(LIBS)

clean :
	rm *.o

batch.o : batch.cpp ${HEADERS}
	gcc $(INCS) -c batch.cpp
gui.o : gui.cpp ${HEADERS}
	gcc $(INCS) -c gui.cpp
imago.o : imago.cpp ${HEADERS}
	gcc $(INCS) -c imago.cpp
param.o : param.cpp ${HEADERS}
	gcc $(INCS) -c param.cpp
pdf.o : pdf.cpp ${HEADERS}
	gcc $(INCS) -c pdf.cpp
