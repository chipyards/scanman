// classe pour fenetre auxiliaire non-modale
class param_view {
public :
// elements du GUI GTK
GtkWidget * wmain;	// fenetre principale
GtkWidget * vsli;
GtkAdjustment *   adk;
GtkAdjustment *   adtop;
GtkAdjustment *   adgam;
GtkAdjustment *   adbot;
GtkAdjustment *   adx0;
GtkAdjustment *   adx1;
GtkAdjustment *   ady0;
GtkAdjustment *   ady1;

// constructeur
param_view() : wmain(NULL) {};

// methodes
void build();
void show();
void hide();

};

class glostru;
extern glostru theglo;
