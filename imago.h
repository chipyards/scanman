class imago {
public:
GdkPixbuf * pix1;	// image originale
GdkPixbuf * pix2;	// image retouchee
unsigned int w1;	// dimensions original
unsigned int h1;
// crop (donne sous forme de marges, 0, 0, 0, 0 <==> no crop)
int x0;
int x1;
int y0;
int y1;
// rotation CCW (0, 90, 180, 270)
int angle;
// levels
int in_Rlo;
int in_Rhi;
int in_Glo;
int in_Ghi;
int in_Blo;
int in_Bhi;
int grayflag;
double expon;	// 1.0/gamma
// tables pour les levels
unsigned char Rlut[256];
unsigned char Glut[256];
unsigned char Blut[256];

// constructeur
imago() : pix1(NULL), pix2(NULL), x0(0), x1(0), y0(0), y1(0), angle(0),
in_Rlo(0), in_Rhi(255),
in_Glo(0), in_Ghi(255),
in_Blo(0), in_Bhi(255),
grayflag(0), expon( 1.0 ) {
	init3lut( 1 );
	};

// methodes
int process();				// traitement pix1 -> pix2
int read( const char * fnam );		// lire pix1 (rend 0 si ok)
int save_png( const char * fnam );	// sauver pix2 (rend 0 si ok)
// void init_lut( unsigned char lut[], int in_lo, int in_hi );
void init_lut( unsigned char lut[], int in_lo, int in_hi, double expon ); // corr gamma avec expon = 1 / gamma
void init3lut( int RcopytoGB );		// init les 3 LUT differentes, ou egales a R
int min_scan( int x0, int y0, int x1, int y1 );	// rend le min sur une zone
};
