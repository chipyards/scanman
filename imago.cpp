#include <gtk/gtk.h>
#include <stdio.h>
#include <math.h>

#include "imago.h"

// traitement pix1 -> pix2 derive de PNG2PDF_02
int imago::process()
{
GdkPixbuf * tmp1;	// image tmp
// etape 1 : crop	// N.B. ceci ne fait pas une copie des pixels, la nouvelle struct reference les anciens pixels
if	( x0 | x1 | y0 | y1 )
	{
	tmp1 = gdk_pixbuf_new_subpixbuf( pix1, x0, y0, w1 - x0 - x1, h1 - y0 - y1 );
	printf("crop done...");
	if	( tmp1 == NULL )
		return -1;
	}
else	tmp1 = pix1;
// etape 2 : rotation	// N.B. ceci fait une copie des pixels et de la struct
if	( pix2 )
	gdk_pixbuf_unref( pix2 );
if	( angle )
	{
	pix2 = gdk_pixbuf_rotate_simple( tmp1, (GdkPixbufRotation)angle );
	printf("rotation done...");
	if	( pix2 == NULL )
		return -2;
	}
else	{
	pix2 = gdk_pixbuf_copy( tmp1 );	// faire une copie pour ne pas ecraser pix1
	printf("copy done...");
	}
// etape 3 : levels & gray	// N.B. ceci ne copie rien
if	(
	( in_Rlo | in_Glo | in_Blo | grayflag ) || ( in_Rhi != 255 ) || ( in_Ghi != 255 ) || ( in_Bhi != 255 ) || ( expon != 1.0 )
	)
	{
	unsigned int iw, ih, ich, istride, x, y, a; unsigned char * pdata;
	iw = gdk_pixbuf_get_width( pix2 );
	ih = gdk_pixbuf_get_height( pix2 );
	ich = gdk_pixbuf_get_n_channels( pix2 );
	istride = gdk_pixbuf_get_rowstride( pix2 );
	pdata = gdk_pixbuf_get_pixels( pix2 );
	if	( grayflag )
		{
		unsigned int lumy;
		for	( y = 0; y < ih; ++y )
			{
			for	( x = 0; x < iw; ++x )
				{
				a = y * istride + x * ich;
				lumy = Rlut[pdata[a]] + Glut[pdata[a+1]] + Blut[pdata[a+2]]; // simple somme
				lumy /= 3;				// c'est pour du texte, pas des photos...
				pdata[a+2] = pdata[a+1] = pdata[a] = (unsigned char)lumy;
				}
			}
		printf("levels/gray done...");
		}
	else	{
		for	( y = 0; y < ih; ++y )
			{
			for	( x = 0; x < iw; ++x )
				{
				a = y * istride + x * ich;
				     pdata[a] = Rlut[pdata[a]]; // R
				++a; pdata[a] = Glut[pdata[a]]; // G
				++a; pdata[a] = Blut[pdata[a]]; // B
				}
			}
		printf("levels done...");
		}
	}
printf("\n"); fflush( stdout );
if	( tmp1 != pix1 )
	gdk_pixbuf_unref( tmp1 );
return 0;
}

// lire pix1
int imago::read( const char * fnam )
{
if	( pix1 )
	gdk_pixbuf_unref( pix1 );
pix1 = gdk_pixbuf_new_from_file( fnam, NULL);
if	( pix1 == NULL )
	return 1;
w1 = gdk_pixbuf_get_width(pix1);
h1 = gdk_pixbuf_get_height(pix1);
return 0;
}

// sauver pix2
int imago::save_png( const char * fnam )
{
if	( pix2 == NULL )
	return 2;
if	( gdk_pixbuf_save( pix2, fnam, "png", NULL, NULL ) )
	{ printf("saved %s\n", fnam ); fflush(stdout); }
else	{ printf("NOT saved %s\n", fnam ); fflush(stdout); return 1; }
return 0;
}

/** petit memo sur le gamma :

Dans le vocabulaire de la TV, une correction de gamma de G equivault a un exposant E = 1/G,
i.e. on veut corriger un display dont le gamma est G.

L'exponentiation �tant appliquee sur une grandeur normalisee sur [0, 1], il n'y a pas
d'effet d'echelle, i.e. le noir et le blanc ne bougent pas.

expression testee avec ffmpeg ( apres application de levels ) :
yout = pow ( yin/255, E ) * 255;

avec E > 1, les valeurs medianes sont abaissees (renforcer un texte a l'encre palie)
avec G > 1, les valeurs medianes sont relevees (eclaircir les zone d'ombre d'une photo)

- ffmpeg exprime la correction au moyen de E
- photoshop exprime la correction au moyen de G
- sur le graphique, photoshop exprime la correction par la position Yg dans la plage d'entree
  de ce qui sera la valeur mediane dans la plage de sortie, t.q. pow( Yg, E ) = 0.5

ref. : Dropbox\MYJOBS\STUDIO\IMAGE\PS8\myjob18.ps8\myjob_levels.ps8
**/

/* calcul lut lineaire
void imago::init_lut( unsigned char lut[], int in_lo, int in_hi )
{
int i; double v;
for ( i = 0; i < 256; i++ )
    {
    v = (double)(i - in_lo);
    v *= ( 255.0 / ((double)(in_hi-in_lo)) );
    if ( v < 0.0 ) v = 0.0;
    if ( v > 255.0 ) v = 255.0;
    v = round(v);
    lut[i] = (unsigned char)v;
    // printf("%d ", lut[i] );
    }
}
*/

// calcul lut avance : correction de gamma avec expon = 1 / gamma
void imago::init_lut( unsigned char lut[], int in_lo, int in_hi, double expon )
{
int i; double v;
for	( i = 0; i < 256; i++ )
	{
	v = (double)( i - in_lo ) / (double)( in_hi - in_lo );
	if ( v < 0.0 ) v = 0.0;
	if ( v > 1.0 ) v = 1.0;
	v = pow( v, expon );
	v *= 255.0;
	v = round(v);
	lut[i] = (unsigned char)v;
	// printf("%3d : %d ", i, lut[i] );
	}
}

// init les 3 LUT differentes, ou egales a R
void imago::init3lut( int RcopytoGB )
{
init_lut( Rlut, in_Rlo, in_Rhi, expon );
if	( RcopytoGB )
	{
	init_lut( Glut, in_Rlo, in_Rhi, expon );
	init_lut( Blut, in_Rlo, in_Rhi, expon );
	}
else	{
	init_lut( Glut, in_Glo, in_Ghi, expon );
	init_lut( Blut, in_Blo, in_Bhi, expon );
	}
}

// rend le min de R, G et B sur une zone
int imago::min_scan( int x0, int y0, int x1, int y1 )
{
int lemin = 255, luma;
int ich, istride, x, y, a; unsigned char * pdata;
ich = gdk_pixbuf_get_n_channels( pix1 );
istride = gdk_pixbuf_get_rowstride( pix1 );
pdata = gdk_pixbuf_get_pixels( pix1 );
if	( grayflag )
	{
	for	( y = y0; y < y1; ++y )
		{
		for	( x = x0; x < x1; ++x )
			{
			a = y * istride + x * ich;
			luma =  ( pdata[a] +  pdata[a+1] + pdata[a+2] ) / 3;
			if	( luma < lemin ) lemin = luma;
			}
		}
	}
else	{
	for	( y = y0; y < y1; ++y )
		{
		for	( x = x0; x < x1; ++x )
			{
			a = y * istride + x * ich;
			if	( pdata[a]   < lemin ) lemin = pdata[a];	// R
			if	( pdata[a+1] < lemin ) lemin = pdata[a+1];	// G
			if	( pdata[a+2] < lemin ) lemin = pdata[a+2];	// B

			}
		}
	}

return lemin;
}

/** petit memo sur le gamma :

Dans le vocabulaire de la TV, une correction de gamma de G equivault a un exposant E = 1/G,
i.e. on veut corriger un display dont le gamma est G.

L'exponentiation �tant appliquee sur une grandeur normalisee sur [0, 1], il n'y a pas
d'effet d'echelle, i.e. le noir et le blanc ne bougent pas.

expression testee avec ffmpeg ( apres application de levels ) :
yout = pow ( yin/255, E ) * 255;

avec E > 1, les valeurs medianes sont abaissees (renforcer un texte a l'encre palie)
avec G > 1, les valeurs medianes sont relevees (eclaircir les zone d'ombre d'une photo)

- ffmpeg exprime la correction au moyen de E
- photoshop exprime la correction au moyen de G
- sur le graphique, photoshop exprime la correction par la position Yg dans la plage d'entree
  de ce qui sera la valeur mediane dans la plage de sortie, t.q. pow( Yg, E ) = 0.5

**/
