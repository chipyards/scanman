// derive de PNG2PDF_02 derive de CAIPDFUX2
#include <gdk/gdk.h>
#include <cairo.h>
#include <cairo-pdf.h>

#include "pdf.h"

int pdfout::init( const char * fnam )
{
mm_margin = 5;
if	( landscape )
	surface = cairo_pdf_surface_create( fnam, A4_H, A4_W );
else	surface = cairo_pdf_surface_create( fnam, A4_W, A4_H );

cairo_status_t status = cairo_surface_status( surface );
if	( status )
	{
	printf( "Failed to create pdf surface : %s\n", cairo_status_to_string(status) );
	return 3;
	}
cai = cairo_create( surface );
return 0;
}

void pdfout::draw_one_page( GdkPixbuf * pixbu )
{
// 1. calculer le cadrage (mise a l'echelle et centrage)
int iw, ih;
double pt_marge, pt_w, pt_h, pt_x0, pt_y0;
double kx, ky, k;
if	( landscape )
	{ pt_w = A4_H; pt_h = A4_W; }
else	{ pt_w = A4_W; pt_h = A4_H; }
pt_marge = mm_margin * MM2PT;
iw = gdk_pixbuf_get_width( pixbu );
ih = gdk_pixbuf_get_height( pixbu );
kx = ( pt_w - ( 2 * pt_marge ) ) / (double)iw;
ky = ( pt_h - ( 2 * pt_marge ) ) / (double)ih;
if	( kx < ky )
	k = kx;
else	k = ky;
pt_x0 = ( pt_w - ( k * iw ) ) * 0.5;
pt_y0 = ( pt_h - ( k * ih ) ) * 0.5;
// 2. tracer l'image
cairo_save( cai );
cairo_translate( cai, pt_x0, pt_y0 );
cairo_scale( cai, k, k );
// N.B. l'operation ci-dessous doit etre faite apres le translate et le scale
// pour que l'image remplisse bien le rectangle
gdk_cairo_set_source_pixbuf( cai, pixbu, 0.0, 0.0 );
// ci-dessous mettre toujours 0.0, 0.0 si on ne veut pas rogner l'image
cairo_rectangle( cai, 0.0, 0.0, (double)iw, (double)ih );
cairo_fill( cai );
cairo_restore( cai );
cairo_show_page( cai );
}

void pdfout::close()
{
cairo_destroy( cai );
cairo_surface_destroy( surface );
}
