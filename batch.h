class batcho {
public:
unsigned int index;
vector <string> lines;

// constructeur
batcho() {};

// methodes
int start_with_list( const char * listpath );	// rend 0 si ok
const char * get_in_name();	// generer un nom de fichier a lire
const char * get_out_name();	// generer un nom de fichier a ecrire
int next();			// avancer
int previous();			// reculer
};
