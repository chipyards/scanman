using namespace std;
#include <string>
#include <vector>

#include <gtk/gtk.h>
#include <stdio.h>
#include <math.h>
// #include "modpop.h"

#include "batch.h"
#include "imago.h"
#include "pdf.h"
#include "param.h"
#include "gui.h"
#include "cli_parse.h"

void start_new_image( glostru * glo )
{
char title[64]; const char * fnam;
fnam = glo->bat.get_in_name();
snprintf( title, sizeof(title), "ScanMan %d/%u : %s", glo->bat.index+1, glo->bat.lines.size(), fnam );
gtk_window_set_title( GTK_WINDOW(glo->wmain), title );
glo->img.read( fnam );
glo->img.process();
glo->darea_queue_flag = 1;
}

/** ============================ call backs ======================= */

gint close_event_call( GtkWidget *widget,
                        GdkEvent  *event,
                        gpointer   data )
{ gtk_main_quit(); return TRUE; }

void param_call( GtkWidget *widget, glostru * glo )
{
glo->para.show();
}

void reset_call( GtkWidget *widget, glostru * glo )
{
gtk_adjustment_set_value( glo->para.adx0, 0 );
gtk_adjustment_set_value( glo->para.ady0, 0 );
gtk_adjustment_set_value( glo->para.adx1, 0 );
gtk_adjustment_set_value( glo->para.ady1, 0 );
}

void prev_call( GtkWidget *widget, glostru * glo )
{
glo->bat.previous();
start_new_image( glo );
}

void next_call( GtkWidget *widget, glostru * glo )
{
printf("in next call\n"); fflush(stdout);
glo->bat.next();
printf("ready for %d->%s\n", glo->bat.index, glo->bat.get_in_name() ); fflush(stdout);
start_new_image( glo );
}

void save_call( GtkWidget *widget, glostru * glo )
{
if	( glo->option_pdf )
	glo->pdfo.draw_one_page( glo->img.pix2 );
else	glo->img.save_png( glo->bat.get_out_name() );
next_call( widget, glo );
}

int idle_call( glostru * glo )
{
if	( glo->darea_queue_flag )
	{
	gtk_widget_queue_draw( glo->darea1 );
	gtk_widget_queue_draw( glo->darea2 );
	glo->darea_queue_flag = 0;
	}

return( -1 );
}


static gboolean expose1_call( GtkWidget	* widget, GdkEventExpose * event, glostru * glo )
{
if	( glo->img.pix1 == NULL )
	{ return FALSE; }
cairo_t *carioc = gdk_cairo_create(widget->window);
cairo_save( carioc );

// centrer dans la drawing-area, en anticipant l'effet de k sur l'image
int ww, wh;
double dx, dy;
gdk_drawable_get_size( widget->window, &ww, &wh );
dx = 0.5 * ( (double)ww - ( glo->k * (double)glo->img.w1 ) );
dy = 0.5 * ( (double)wh - ( glo->k * (double)glo->img.h1 ) );
cairo_translate( carioc, dx, dy );

// ce scale va affecter le pattern aussi bien que le trace du rectangle le cas echeant
cairo_scale( carioc, glo->k, glo->k );

// affecter notre pixbuf au pattern de remplissage courant
// Hum... "further modifications of the current transformation matrix will not affect the source pattern"
gdk_cairo_set_source_pixbuf( carioc, glo->img.pix1, 0, 0 );
// alors cairo_get_source() peut nous rendre le cairo_pattern_t
// configurer les bords (par defaut CAIRO_EXTEND_NONE = pixels transparents en dehors du pattern )
// cairo_pattern_set_extend( cairo_get_source(carioc), CAIRO_EXTEND_REPEAT );
// configurer la matrice de transform (par defaut c'est unite)
// cairo_pattern_set_matrix( cairo_get_source(carioc), &matrix );
// configurer le filtrage (seems CAIRO_FILTER_BILINEAR by default)
// cairo_pattern_set_filter( cairo_get_source(carioc), CAIRO_FILTER_NEAREST );

// tracer un rectangle de la bonne taille
//cairo_rectangle( carioc, 0.0, 0.0,
//		   glo->k * (double)gdk_pixbuf_get_width(glo->backpix),
//		   glo->k * (double)gdk_pixbuf_get_height(glo->backpix) );
//cairo_fill(carioc);

// beaucoup plus simple, applique le pattern sur toute la drawing-area
cairo_paint(carioc);

// le rectangle fantome du mouse_drag
cairo_restore( carioc );	// sinon il ne suit pas la mouse quand k != 1.0
if	( glo->drag_fill )
	{			// inutilise pour le momaent
	cairo_set_source_rgba( carioc, 0.0, 0.8, 1.0, 0.3 );
	cairo_rectangle( carioc, glo->x0, glo->y0, glo->x1 - glo->x0, glo->y1 - glo->y0 );
	cairo_fill(carioc);
	}
else if	( glo->drag_stroke )
	{			// crop
	cairo_set_source_rgba( carioc, 1.0, 0.7, 0.0, 0.8 );
	cairo_rectangle( carioc, glo->x0, glo->y0, glo->x1 - glo->x0, glo->y1 - glo->y0 );
	cairo_stroke(carioc);
	}

cairo_destroy(carioc);
return FALSE;	// MAIS POURQUOI ???
}

static gboolean expose2_call( GtkWidget	* widget, GdkEventExpose * event, glostru * glo )
{
if	( glo->img.pix2 == NULL )
	{ return FALSE; }
cairo_t *carioc = gdk_cairo_create(widget->window);

// centrer dans la drawing-area, en anticipant l'effet de k sur l'image
int ww, wh;
double dx, dy;
gdk_drawable_get_size( widget->window, &ww, &wh );
dx = 0.5 * ( (double)ww - ( glo->k * (double)gdk_pixbuf_get_width(glo->img.pix2) ) );
dy = 0.5 * ( (double)wh - ( glo->k * (double)gdk_pixbuf_get_height(glo->img.pix2) ) );
cairo_translate( carioc, dx, dy );

// ce scale va affecter le pattern aussi bien que le trace du rectangle le cas echeant
cairo_scale( carioc, glo->k, glo->k );
// affecter notre pixbuf au pattern de remplissage courant
gdk_cairo_set_source_pixbuf( carioc, glo->img.pix2, 0, 0 );
// remplir
cairo_paint(carioc);

cairo_destroy(carioc);
return FALSE;
}

static gboolean configure1_call( GtkWidget * widget, GdkEventConfigure * event, glostru * glo)
{
// printf("configuzed 1\n");
return TRUE;
}

static gboolean configure2_call( GtkWidget * widget, GdkEventConfigure * event, glostru * glo)
{
// printf("configuzed 2\n");
return TRUE;
}

static gboolean click_call( GtkWidget * widget, GdkEventButton * event, glostru * glo )
{
if	( event->type == GDK_BUTTON_PRESS )
	{
	glo->x0 = event->x;
	glo->y0 = event->y;
	glo->x1 = glo->x0;
	glo->y1 = glo->y0;
	if	( event->button == 1 )
		{
		glo->drag_fill = 1;
		}
	else if	( event->button == 3 )
		{
		glo->drag_stroke = 1;
		}
	}
else if	( event->type == GDK_BUTTON_RELEASE )
	{
	// ici inserer traitement de la fenetre draguee : action select ou action zoom
	if	( ( glo->drag_stroke ) || ( glo->drag_fill ) )
		{
		// trouver l'origine de l'image en coord ecran
		int ww, wh;
		gdk_drawable_get_size( widget->window, &ww, &wh );
		double dx, dy;
		dx = 0.5 * ( (double)ww - ( glo->k * (double)glo->img.w1 ) );
		dy = 0.5 * ( (double)wh - ( glo->k * (double)glo->img.h1 ) );
		// traiter le rectangle de drag
		if	( glo->x0 > glo->x1 )
			{ double tmp = glo->x0; glo->x0 = glo->x1; glo->x1 = tmp;  }
		if	( glo->y0 > glo->y1 )
			{ double tmp = glo->y0; glo->y0 = glo->y1; glo->y1 = tmp;  }
		// changer d'origine
		glo->x0 -= dx;
		glo->y0 -= dy;
		glo->x1 -= dx;
		glo->y1 -= dy;
		// changer d'echelle
		glo->x0 = round( glo->x0 / glo->k );
		glo->y0 = round( glo->y0 / glo->k );
		glo->x1 = round( glo->x1 / glo->k );
		glo->y1 = round( glo->y1 / glo->k );
		// borner
		if	( glo->x0 < 0.0 ) glo->x0 = 0.0;
		if	( glo->y0 < 0.0 ) glo->y0 = 0.0;
		if	( glo->x1 > glo->img.w1 ) glo->x1 = glo->img.w1;
		if	( glo->y1 > glo->img.h1 ) glo->y1 = glo->img.h1;
		if	( glo->drag_stroke )
			{
			// relativifier
			glo->x1 = glo->img.w1 - glo->x1;
			glo->y1 = glo->img.h1 - glo->y1;
			//appliquer
			// printf("x0=%g, y0=%g, x1=%g, y1=%g\n", glo->x0, glo->y0, glo->x1, glo->y1 ); fflush(stdout);
			gtk_adjustment_set_value( glo->para.adx0, glo->x0 );
			gtk_adjustment_set_value( glo->para.ady0, glo->y0 );
			gtk_adjustment_set_value( glo->para.adx1, glo->x1 );
			gtk_adjustment_set_value( glo->para.ady1, glo->y1 );
			}
		else if	( glo->drag_fill )
			{
			int level_top = glo->img.min_scan( (int)glo->x0, (int)glo->y0, (int)glo->x1, (int)glo->y1 );
			printf("level_top = %d\n", level_top ); fflush(stdout);
			if	( level_top > 128 )	// ajuster le blanc
				gtk_adjustment_set_value( glo->para.adtop, level_top );
			else if	( level_top < 80 )	// ajuster le noir
				gtk_adjustment_set_value( glo->para.adbot, level_top );
			}
		glo->drag_fill = 0;
		glo->drag_stroke = 0;
		gtk_widget_queue_draw( glo->darea1 );
		}
	}
/* We've handled the event, stop processing */
return TRUE;
}

static gboolean motion_call( GtkWidget * widget, GdkEventMotion	* event, glostru * glo )
{
GdkModifierType state;

/* le truc de base, si on utilise pas le hint */
glo->x1 = (int)event->x; glo->y1 = (int)event->y; state = (GdkModifierType)event->state;

if   ( ( state & GDK_BUTTON1_MASK ) || ( state & GDK_BUTTON3_MASK ) )
     {
     glo->darea_queue_flag = 1;
     }

/* We've handled it, stop processing */
return TRUE;
}

static gboolean wheel_call( GtkWidget * widget, GdkEventScroll * event, glostru * glo )
{
if	( event->direction == GDK_SCROLL_DOWN )
	{
	gtk_adjustment_set_value( glo->para.adk, glo->k + 0.1 );
	}
else if	( event->direction == GDK_SCROLL_UP )
	{
	if	( glo->k > 0.1 )
	gtk_adjustment_set_value( glo->para.adk, glo->k - 0.1 );
	}
glo->darea_queue_flag = 1;
return TRUE;	// We've handled it, stop processing
}

/** ============================ constr. GUI ======================= */

void usage()
{
printf("usage : scanman <options> <liste.txt>\n"
	"options :\n"
	" -G : force grayscale\n"
	" -L : PDF orientation landscape (defaut = portrait)\n"
	" -P myfile.pdf : sortie PDF multi-pages (defaut sortie PNG dans ./OUT/)\n"
	"pour sortie PNG, la liste doit etre en relatif pour insertion prefixe OUT\n");
}

glostru theglo;

int main( int argc, char *argv[] )
{
#define glo (&theglo)
GtkWidget *curwidg;

setlocale( LC_ALL, "C" );	// question de survie

if	( argc <= 1 )
	{ usage(); return 0; }

// 	parsage CLI
cli_parse * lepar = new cli_parse( argc, (const char **)argv, "P" );
// le parsage est fait, on recupere les args !
const char * val;
if	( ( val = lepar->get( 'L' ) ) )
	glo->pdfo.landscape = 1;	// a faire AVANT pdfo.init( val )
if	( ( val = lepar->get( 'P' ) ) )
	{
	glo->option_pdf = 1;
	if	( glo->pdfo.init( val ) )
		return 3;
	}
if	( ( val = lepar->get( 'G' ) ) )
	glo->img.grayflag = 1;

val = lepar->get( '@' );		// get avec la clef '@' rend la chaine nue
// lire la liste
if	( glo->bat.start_with_list( val ) )
	return 1;
if	( glo->bat.lines.size() == 0 )
	return 2;

/* test crop *
glo->img.x1 = 100; glo->img.y1 = 200;
//*/
/* test rotation *
glo->img.angle = 90;
//*/
/* test levels *
glo->img.in_Rlo = glo->img.in_Glo = glo->img.in_Blo = 40;
glo->img.in_Rhi = glo->img.in_Ghi = glo->img.in_Bhi = 220;
glo->img.init3lut( 1 );
//*/
/* test grayscale *
glo->img.grayflag = 1;
//*/

gtk_init(&argc,&argv);

curwidg = gtk_window_new( GTK_WINDOW_TOPLEVEL );

// pour garder le controle de la situation (bouton X du bandeau de la fenetre)
g_signal_connect( curwidg, "delete_event",
		  G_CALLBACK( close_event_call ), glo );
// juste pour le cas ou on aurait un gasp() qui destroy abruptment la fenetre principale
g_signal_connect( curwidg, "destroy",
		  G_CALLBACK( gtk_main_quit ), NULL );

gtk_window_set_title( GTK_WINDOW (curwidg), "Scan Man");
gtk_container_set_border_width( GTK_CONTAINER( curwidg ), 10 );
glo->wmain = curwidg;

/* creer boite verticale */
curwidg = gtk_vbox_new( FALSE, 5 ); /* spacing ENTRE objets */
gtk_container_add( GTK_CONTAINER( glo->wmain ), curwidg );
glo->vmain = curwidg;

/* Create the expander *
curwidg = gtk_expander_new("Sliders");
gtk_box_pack_start( GTK_BOX(glo->vmain), curwidg, FALSE, FALSE, 0);
glo->esli = curwidg;
*/
/* creer boite verticale *
curwidg = gtk_vbox_new( FALSE, 5 );
gtk_container_add( GTK_CONTAINER( glo->esli ), curwidg );
glo->vsli = curwidg;
*/
// paire horizontale "paned"
curwidg = gtk_hpaned_new ();
gtk_box_pack_start( GTK_BOX( glo->vmain ), curwidg, TRUE, TRUE, 0 );
glo->hpan = curwidg;

// creer une drawing area
curwidg = gtk_drawing_area_new ();
// set a minimum size
gtk_widget_set_size_request (curwidg, 400, 400);
gtk_paned_pack1( GTK_PANED(glo->hpan), curwidg, TRUE, FALSE );
g_signal_connect( curwidg, "expose_event", G_CALLBACK(expose1_call), glo );
g_signal_connect( curwidg, "configure_event", G_CALLBACK(configure1_call), glo );
glo->darea1 = curwidg;

// creer une drawing area
curwidg = gtk_drawing_area_new ();
// set a minimum size
gtk_widget_set_size_request (curwidg, 400, 400);
gtk_paned_pack2( GTK_PANED(glo->hpan), curwidg, TRUE, FALSE );
g_signal_connect( curwidg, "expose_event", G_CALLBACK(expose2_call), glo );
g_signal_connect( curwidg, "configure_event", G_CALLBACK(configure2_call), glo );
glo->darea2 = curwidg;

// Mouse Signals (darea1 only)
g_signal_connect( glo->darea1, "button_press_event",   G_CALLBACK(click_call), glo );
g_signal_connect( glo->darea1, "button_release_event", G_CALLBACK(click_call), glo );
g_signal_connect( glo->darea1, "motion_notify_event", G_CALLBACK( motion_call ), glo );
g_signal_connect( glo->darea1, "scroll_event", G_CALLBACK(wheel_call), glo );

// Ask to receive events the drawing area doesn't normally subscribe to
gtk_widget_set_events( glo->darea1, gtk_widget_get_events(glo->darea1)
			| GDK_BUTTON_PRESS_MASK
			| GDK_BUTTON_RELEASE_MASK
			| GDK_POINTER_MOTION_MASK
			| GDK_SCROLL_MASK
		     );
glo->drag_fill = 0;
glo->drag_stroke = 0;

/* creer boite horizontale */
curwidg = gtk_hbox_new( FALSE, 10 ); /* spacing ENTRE objets */
gtk_container_set_border_width( GTK_CONTAINER (curwidg), 5);
gtk_box_pack_start( GTK_BOX( glo->vmain ), curwidg, FALSE, FALSE, 0 );
glo->hbut = curwidg;

/* simple bouton */
curwidg = gtk_button_new_with_label (" Param ");
gtk_signal_connect( GTK_OBJECT(curwidg), "clicked",
                    GTK_SIGNAL_FUNC( param_call ), (gpointer)glo );
gtk_box_pack_start( GTK_BOX( glo->hbut ), curwidg, TRUE, TRUE, 0 );

/* simple bouton */
curwidg = gtk_button_new_with_label (" Reset crop ");
gtk_signal_connect( GTK_OBJECT(curwidg), "clicked",
                    GTK_SIGNAL_FUNC( reset_call ), (gpointer)glo );
gtk_box_pack_start( GTK_BOX( glo->hbut ), curwidg, TRUE, TRUE, 0 );

/* simple bouton */
curwidg = gtk_button_new_with_label (" Previous ");
gtk_signal_connect( GTK_OBJECT(curwidg), "clicked",
                    GTK_SIGNAL_FUNC( prev_call ), (gpointer)glo );
gtk_box_pack_start( GTK_BOX( glo->hbut ), curwidg, TRUE, TRUE, 0 );

/* simple bouton */
curwidg = gtk_button_new_with_label (" Save ");
gtk_signal_connect( GTK_OBJECT(curwidg), "clicked",
                    GTK_SIGNAL_FUNC( save_call ), (gpointer)glo );
gtk_box_pack_start( GTK_BOX( glo->hbut ), curwidg, TRUE, TRUE, 0 );

/* simple bouton */
curwidg = gtk_button_new_with_label (" Next ");
gtk_signal_connect( GTK_OBJECT(curwidg), "clicked",
                    GTK_SIGNAL_FUNC( next_call ), (gpointer)glo );
gtk_box_pack_start( GTK_BOX( glo->hbut ), curwidg, TRUE, TRUE, 0 );

// lire fichier 1ere image
start_new_image( glo );

gtk_widget_show_all( glo->wmain );

// preparer le layout de la fenetre non modale 'param'
glo->para.build();
// on la show meme
// glo->para.show();

g_timeout_add( 31, (GSourceFunc)(idle_call), (gpointer)glo );

gtk_main();

if	( glo->option_pdf )
	glo->pdfo.close();

return(0);
}
