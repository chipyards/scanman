using namespace std;
#include <string>
#include <vector>

#include <stdio.h>
#include <string.h>
#include "batch.h"

// rend 0 si ok
int batcho::start_with_list( const char * listpath )
{
FILE * fil;
fil = fopen( listpath, "r" );
if	( fil == NULL )
	return 1;
char lbuf[256]; int pos, len;
while	( fgets( lbuf, sizeof( lbuf ), fil ) )	// aucune confiance dans ce fgets
	{
	lbuf[sizeof(lbuf)-1] = 0;		// il peut omettre le terminateur
	len = (int)strlen(lbuf);
	// on attaque par la fin pour enlever line end et trailing blank
	pos = len - 1;
	while	( ( pos >= 0 ) && ( lbuf[pos] <= ' ' ) ) // ou rendre une chaine vide
		lbuf[pos--] = 0;			 // on enleve line end et trailing blank
	lines.push_back( string( lbuf ) );
	}
fclose( fil );
index = 0;
return 0;
}

// generer un nom de fichier a lire
const char * batcho::get_in_name()
{
if	( lines.size() )
	return lines.at(index).c_str();
else	return "noname";
}

// generer un nom de fichier a ecrire
const char * batcho::get_out_name()
{
static string fnam;
if	( lines.size() )
	fnam = "OUT\\" + lines.at(index);	// + ".png";
return fnam.c_str();
}

// avancer, rend index
int batcho::next()
{
if	( ++index >= lines.size() )
	index = lines.size() - 1;
return (int)index;
}

// reculer, rend index
int batcho::previous()
{
if	( index > 0 )
	--index;
return (int)index;
}
